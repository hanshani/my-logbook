| **Head** | **Information** |
| ------ | ----------- |
| Logbook entry number | 01 |
| Name   | Hani Abdullanahadi |
| Matrix number | 195456 |
| Year   | 4 (final year) |
| Subsystem   | Propulsion and Power |
| Group | 2 |
| Date   | 25/10/2021-29/10/2021 |


| **Target** | **Details** |
| ------ | ----------- |
| Agenda   | <br>- Learned how to work on Gitlab, visual code, and writing Markdown.<br>- Formed out the subsystem group's Gantt chart.<br>- Discussed the timeline of the progress on thrust test ( Power Analysis, Thrust, Endurance, Temperature, Drag, RPM). | 
| Goals | <br>- Be familialize with Gitlab program and writing Markdown.<br>- Created the Gantt chart until week14 <br>- Achieved exeicises that had been assigned.<br>- Made an appointment with Dr.Ezanee in order to use the lab H2.3 to perform thrust test. |
| Decisions | <br>- Decided on what we (Propulsion and Power subsystem) to do each week and put in Gantt chart.<br>- Decided to send only few of team members to go to the lab due to the pandemic.<br>- Team up with friends to work on Gitlab to be more famiilialize.|
| Method   | <br>- Made a duty schedule to see who free on which day.<br>- Team members volunteered to go to the lab after get the exact day. |
| Justification when solving problem | 2 |
| Impact   | <br>- Gitlab was new for us to work with.<br>- Limitation of team members who would able to go to the lab due to the pandemic. |
| Next step | <br>- Performing thrust test at the lab H2.3 |
